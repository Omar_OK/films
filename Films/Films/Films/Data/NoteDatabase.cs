﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Films.Models;

using Xamarin.Forms;

namespace Films.Data
{
	public class NoteDatabase
	{
        readonly SQLiteAsyncConnection _database;

        public NoteDatabase(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Film>().Wait();
        }

        public Task<List<Film>> GetNotesAsync()
        {
            return _database.Table<Film>().ToListAsync();
        }

        public Task<Film> GetNoteAsync(int id)
        {
            return _database.Table<Film>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        public Task<int> SaveNoteAsync(Film film)
        {
            if (film.ID != 0)
            {
                return _database.UpdateAsync(film);
            }
            else
            {
                return _database.InsertAsync(film);
            }
        }

        public Task<int> DeleteNoteAsync(Film film)
        {
            return _database.DeleteAsync(film);
        }
    }
}